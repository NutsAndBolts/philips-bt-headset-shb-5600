// File for constants (and constraints) used in the project.
// Wire thickness, can be changed at compile time
// Thickness
thick = 1.6;
// Hood stage 1
stage1 = [75, 0, 3.2];
// Hood stage 2
stage2 = [59.62, 0, 15.3];
// Max height
height = stage2.z;
// Rod dimensions and angle
rod = [5, 0, height - 4];
rodGroove = 12;
rodRiftAngle = [0, 0, 150];
// Centerings dimensions;
centering = [4, 0.4, 10];
centeringFwd = 2;
// Clips
clipsNumber =  4;
clipsForward = 4;
clips = [2, 5, 2];
// Distance between the top of the hull, and the clips
clipsTop = 10 + thick;

// Vertical positioning of the microUSB hole from the top of the Hood
USBHeight = 6;
USBAngle = [0, 0, 180];
// Clips and centering Angle differs between left and right.
centeringAngleLeft = [0, 0, 45];
clipsAngleLeft = centeringAngleLeft + [0, 0, 45];
rodAngleLeft = [0, 0, 70];
centeringAngleRight = [0, 0, 75];
clipsAngleRight = centeringAngleRight + [0, 0, 45];
rodAngleRight = [0, 0, 105];

// Dimensions of the hull containing the volume button
volume = [13, 6, 3.7];
volumeAngle = clipsAngleRight + [0, 0, 22.5];
volumeInsert = 2.5;

// Battery holder
battery = [33, 17];
batteryOffset = [0, 10, 0];
// PCB size
card = [14, 32.5, stage2.x - 2 * thick - 20];
cardAngle = [0, 0, 135];
cardOffset = [0, 0, 5];

// Mic holder
mic = [4, 7, 6];
micHole = [2, 0.2, thick + 0.2];
micAngle = [0, 0, 240];
micOffset = [stage2.x/2, 0, stage2.z/2];
