// Import and use
use <MCAD/gears.scad>;
use <text_on/text_on.scad>;
use <okhin.scad>;
use <CC_logos.scad>

include <main.scad>;
include <constants.scad>;

// Uncomment those to have nicer circles
//$fs = 1;
//$fa = 0.5;
MicHolder(hole=micHole, holder=mic, a=micAngle, o=micOffset)
difference() {
    VolumeGroove(a=volumeAngle, h=stage2.z - thick, e=thick, d=volumeInsert, W=volume.x + volume.y + volumeInsert, D=stage2.x + thick)
        Rod(a=rodAngleRight, e=thick, d=rod.x, w=stage2.x, h=rod.z, a_rift=rodRiftAngle)
        BatteryHolder(w=battery.x, h=battery.y, e=thick, t=batteryOffset.y, z=stage2.z - thick)
        Hood(base=[stage1.x, stage1.z], hood=[stage2.x, stage2.z], e=thick, rod=rodGroove);
    rotate(USBAngle)
        MicroUSBInsert(e=thick, d=stage2.x, h=height - USBHeight);
    translate([0, 0, height - volume.z - volume.y / 2])
    rotate(volumeAngle)
        VolumeSlot(d=volume.y, w=volume.x, D=stage2.x, e=thick);
}

rotate(clipsAngleRight)
translate([0, 0, -clipsTop + stage1.z])
    MaleClips(w=stage2.x - 2 * thick -  2 * clips.x, e=thick, fwd=clipsForward, n=clipsNumber, p=clips.x, l=clips.y, h=height - stage1.z);

translate([0, 0, height - centering.z - thick])
    rotate(centeringAngleRight)
    Centering(d = centering.x, e=thick/2, h=centering.z, fwd=centeringFwd, w=stage2.x - 2 * thick);

CardSupport(a=cardAngle, dt=4, h=cardOffset.z, d=card.z, e=thick, t=stage2.z - thick - cardOffset.z);
//


// Markings
/*
rotate([0, 180, 0])
translate([0, 0, - (height - thick - 0.5)])
linear_extrude(0.5) {
    translate([0, 4, 0])
        text("R", halign="center", valign="center", size = 8);
    translate([-8, -8, 0])
        cc_domain_public(s = 8);
}
*/
