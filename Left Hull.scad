// Import and use
use <MCAD/gears.scad>;
use <text_on/text_on.scad>;
use <okhin.scad>;
use <CC_logos.scad>;

include <constants.scad>;
include <main.scad>;

Rod(a=rodAngleLeft, e=thick, d=rod.x, w=stage2.x, h=rod.z, a_rift=rodRiftAngle)
Hood(base=[stage1.x, stage1.z], hood=[stage2.x, stage2.z], e=thick, rod=rodGroove);

rotate(clipsAngleLeft)
translate([0, 0, -clipsTop + stage1.z])
MaleClips(w=stage2.x - 2 * thick -  2 * clips.x, e=thick, fwd=clipsForward, n=clipsNumber, p=clips.x, l=clips.y, h=height - stage1.z );

translate([0, 0, height - centering.z - thick])
rotate(centeringAngleLeft)
Centering(d = centering.x, e=centering.y, h=centering.z, fwd=centeringFwd, w=stage2.x - 2 * thick);

// Markings
translate([0, 0, height - thick - 0.5])
rotate([0, 180, 0])
linear_extrude(0.5) {
translate([0, 4, 0])
text("L", halign="center", valign="center", size = 8);
translate([-10, -10, 0])
cc_domain_public(s = 10);
}
