// Import and use
use <MCAD/gears.scad>;
use <text_on/text_on.scad>;
use <okhin.scad>;
use <CC_logos.scad>;

// This creates the hood needed for the hearphone. Holes and stuff needs tobe added later.
module Hood(base=[60, 15], hood=[40, 30], e=1.75) {
    difference() {
        cylinder(d=hood[0], h=hood[1]);
        difference() {
            cylinder(d=hood[0], h=base[1] - e);
            translate([0, 0, (base[1] + rod) / 2])
                rotate([0, 90, 0])
                cylinder(d=rod + 2 * e, h=base[0]);
        };
        minkowski() {
            cylinder(d=hood[0] - 4 * e, h=hood[1] - 2 * e);
            sphere(e);
        }
        // A rod is going through here
    };
    difference() {
        cylinder(d=base[0], h=base[1]);
        translate([0, 0, (base[1] + rod) / 2])
            rotate([0, 90, 0])
            cylinder(d=rod, h=base[0]);
        difference() {
            if (base[1] > 2 * e) {
                minkowski() {
                    sphere(1);
                    resize([base[0] - 4 * e, 0, base[1] - 2 * e], auto = true)
                        cylinder(d=base[0], h=base[1]);
                };
            } else {
                resize([base[0] - 2 * e, 0, base[1] -  e], auto = true)
                    cylinder(d=base[0], h=base[1]);
            };
            translate([0, 0, (base[1] + rod) / 2])
                rotate([0, 90, 0])
                cylinder(d=rod + 2 * e, h=base[0]);
            };
        cylinder(d=hood[0] - 2 * e, h=hood[1]);
        };
};

module MaleClips(p=1, l=5, w=20, n=4, a=[0, 0, 360], h=10, fwd=0) {
    if (fwd > 0) {
        rotate(a - a/n)
        translate([0, w / 2 - fwd - p, h]) {
            translate([0, p, 0])
            linear_extrude(h - 0.5) {
                square([l, 0.4], center = true);
                translate([0, (fwd + p)/ 2, 0])
                    square([0.4, fwd + p ], center = true);
                };
            rotate([0, 90, 0])
            linear_extrude(height = l, center = true)
            polygon(points=[[-p,p],[0, p], [-p, 0]]);
            };
    };

    rotate_params = (fwd > 0) ? [a - 2*a/n, n - 2]: [a, n - 1];
    rotate_copy(a=rotate_params[0], n = rotate_params[1])
        translate([l/2, w / 2, h])
        rotate([0, 90, 0])
        linear_extrude(height = l, center=true)
        polygon(points=[[-p,p],[0, p], [-p, 0]]);
};

module Rod(a=[0, 0, 0], e=1, d=6, w=10, h=10, a_rift=[0, 0, 150]) {
    difference() {
        // Need to build the supportive part first
        union() {
            rotate(a)
            translate([0, 0, h - d / 2 - 1])
            intersection() {
                cube([w, d + 2 * e, h + d - 2 * e], center=true);
                hollow_cylinder(d=w, h=h + d + e, e = 2 * e, center = true);
            };
            children();
        };
        rotate(a) {
        translate([0, 0, h - d/2])
        rotate([0, 90, 0])
        cylinder(d=d, h=w, center=true);
        // There's a need for a rift
        mirror_copy()
        difference() {
            translate([w/2 - e, 0, h - d/2])
            rotate([-90, 0, 0])
            rotate([0, 90, 0])
            linear_extrude(e)
            difference() {
                circle(d=d+2*e);
                translate([-(d+2*e)/2, e / 2])
                    square(d+2*e);
                rotate(a_rift)
                    translate([0, e/2])
                    square(d+2*e);
                };
            cylinder(h=w, d=w - 2*e);
            };
        };
    };
};

module Centering(d=4, e=1, h=10, fw=5, w=30) {
    mirror_copy()
    translate([(w - (fw + d + e))/ 2, 0, h])
        rotate([0, 180, 0]){
        linear_extrude(h/2)
            translate([e - d, 0])
            square([fw - d/2 + e, e], center=true);
        linear_extrude(h)
            hollow_circle(d=d, e=e, h=h);
    }
}

module MicroUSBInsert(e=1, d=20, h=5) {
    translate([d/2 - 2.5, 0, h])
        rotate([90, 0, 90])
        linear_extrude(e*2)
        hull() {
            square([5, 1], center=true);
            mirror_copy()
                translate([2.5, -2])
                circle(1.5);
        };
}

module VolumeSlot(d=6, w=20, D=60, e=1, a=[0, 0, 0]) {
    translate([0.5,0,0])
    intersection() {
        translate([0, 0, -50])
            hollow_cylinder(d=D, h=100, e=2 * e);
        rotate([0, 90, 0])
            linear_extrude(D/2)
            hull() {
                translate([0, - w/2, 0])
                circle(d=d);
                mirror()
                    translate([0, w/2, 0])
                    circle(d=d);
                };
    };
}

module VolumeGroove(a=[0, 0, 0], h=10, d=2, e=1, W=20, D=60) {
    // I know one side and the hypotenusis of a rectangle triangle. h2 = a2 + b2 -> a = sqrt(h2 - b2)
    hypo = sqrt(pow(D/2 - e - d/2, 2) - pow(W/2, 2));
    difference() {
        children();
        rotate(a) {
            mirror_copy(v=[0, -1, 0])
                translate([hypo, W/2])
                cylinder(d=d-0.4, h=h);
            translate([D/4, 0, e/2])
                cube([D/2 - e, e, e], center=true);
            };
        };
    rotate(a) {
        mirror_copy(v=[0, -1, 0])
            translate([hypo, W/2, e])
            hollow_cylinder(d=d, h=h, e=0.2);
        translate([D/2 - e - 7, 0, h - cardOffset.z/2])
            cube([0.5, 3, cardOffset.z], center=true);
    };
}

module BatteryHolder(w=33, h=20, e=1, t=10, z=0) {
    difference() {
        children();
        translate([t, 0, z])
        minkowski() {
            cube([h - e, w - e, 0.1], center=true);
            sphere(d=e/2, center=true);
            };
    };
    translate([t, 0, z - e/2])
        linear_extrude(1) {
        mirror_copy()
            translate([h/2 - e/2 + 0.05, 0])
            square([0.1, 10], center=true);
        mirror_copy(v=[0, -1, 0])
            translate([0, w/2 - e/2 + 0.05])
            square([10, 0.1], center=true);
    }
}

module CardTower(d=5, e=1, h=6, up=false, a=[0, 0, 0]) {
    if (up == true){
        difference() {
            translate([0, 0, -e])
                hollow_cylinder(d=d, h=h + e, e=e);
            translate([0, -d/2, -d])
                cube(d);
            };
    } else {
        hollow_cylinder(d=d, h=h, e=e);
    };
    rotate(a) {
        translate([d/2 + 2 * e + e/2, 0, - 2 * e])
            cylinder(h=h + 2*e, d=e);
        translate([d/2, -e/4, 0])
            cube([2*e, e/2, h]);
    };
}

module CardSupport(a=[0, 0, 45], dt=4, h=5, d=30, e=1, t=0) {
    // There's a small tower to be built first
    translate([0, (d - dt)/2, t])
        rotate([0, 0, 180])
        CardTower(h=h, up=true, d=dt, a=[0, 0, -45]);
    rotate(a)
        translate([0, d/2, t])
        rotate(-a)
        CardTower(h=h, d=dt);
}

module MicHolder(hole=[2, 0, 2], holder=[10, 20, 10], a=[0, 0, 90], o=[20, 0, 10]) {
    // First, let's make a conic hole on the children
    difference() {
        children();
        rotate(a)
            translate(o)
            rotate([0, -90, 0])
            cylinder(h=hole.z, d1=hole.x, d2=hole.y);
    };
    // Now the holder
    rotate(a)
    translate(o - [holder.x, 0, 0])
    difference() {
    linear_extrude(holder.z)
        difference() {
            square([holder.x, holder.y], center=true);
            translate([0.2, 0])
                square([holder.x - 0.2, holder.y - 0.4], center=true);
        };
        rotate([90, 0, -90])
            linear_extrude(holder.x)
            offset(0.2)
            polygon([[-0.4, -0.2],[0, holder.z/2 - 0.2],[0.4, -0.2]]);
    }
}
