use <okhin.scad>;
use <CC_logos.scad>

dBase = 72.5;
eBase = 2.1;
dStructure = 60;
dSpeaker = 32;
hSpeaker = 5;
dChambre = 5;
hChambre = 9.6;
aChambre = 135;
wOuie = 10;
dOuie = 17.5;
aOuie = 30;

// Uncomment those to have nicer circles
$fs = 1;
$fa = 0.5;

difference() {
    cylinder(d = dBase, h = eBase);
    translate([0, 0, eBase -1 ])
        minkowski() {
            cylinder(d = dSpeaker - 4, h = 5, center = false);
            sphere(d = 1);
        };
    rotate([0, 0, 90]) {
        ouie(d=dOuie, a=aOuie/2, w=wOuie, e=eBase/2);
        ouie(d=dOuie + wOuie/4, a=aOuie/2, w=wOuie/2, e=eBase);
    };
    ring_drill(d=3.7, r=2, s=2, e=2);
    // passage de couture
    translate([0, dBase / 2, 0])
        linear_extrude(eBase*5)
        offset(1)
        union() {
            mirror_copy()
                polygon([[0,0], [0,-4], [-1, 0]]);
        };
    };

translate([0, 0, eBase])
    linear_extrude(1, center = false) {
        hollow_circle(d = dStructure, e = 1);
        hollow_circle(d = dSpeaker + 1, e = 1);
        rotate([0, 0, aChambre])
            mirror_copy()
                difference() {
                    union() {
                        translate([(dSpeaker + 2) / 2 - 0.5, -0.5, 0])
                            square([(dStructure - dSpeaker - 2) / 2, 1]);
                        translate([(54 - dChambre + 1) / 2, 0, 0])
                            circle(d = dChambre + 1);
                    };
                    translate([(54 - dChambre + 1) / 2, 0, 0])
                        circle(d = dChambre);
                };
        rotate([0, 0, 65])
            mirror_copy()
            translate([(dSpeaker + 2) / 2 - 0.5, 0, 0])
                square([(dStructure - dSpeaker - 2) / 2, 1]);
        rotate([0, 0, 30])
            mirror_copy()
            translate([(dSpeaker + 2) / 2 - 0.5, 0, 0])
            square([(dStructure - dSpeaker - 2) / 2, 1]);
    };

hollow_cylinder(d = dSpeaker + 2, h = hSpeaker, e = 1);

rotate([0, 0, aChambre])
    mirror_copy()
        translate([(54 - dChambre + 1) / 2, 0, eBase])
            hollow_cylinder(d = dChambre + 1, h = hChambre, e= 1);
rotate([0, 0, 8]) {
    // Clips
    rotate([0, 0, 90])
        rotate_copy(a=[0,0, 180], n=2)
        translate([9.7 + dSpeaker/2, 0, eBase])
                union() {
                    rotate([90, -90, 0])
                    linear_extrude(1, center=false)
                    union() {
                        square([3, 8.7]);
                        translate([3, 0])
                            polygon([[0, 0],
                                [0, 3.5],
                                [5, 1.5],
                                [5, 0]]);
                    };
                    translate([0, 5, 8])
                        rotate([90, 180, 90])
                        linear_extrude(1, center=false)
                        difference() {
                            square([10, 8]);
                            translate([1.5, 2])
                                square([7, 2.5]);
                        };
                };
        // short clip
        translate([6 + dSpeaker/2, 0, eBase])
            union() {
                rotate([90, -90, 0])
                linear_extrude(1, center=false)
                union() {
                    square([3, 5]);
                    translate([3, 0])
                        polygon([[0, 0],
                        [0, 3.5],
                        [5, 1.5],
                        [5, 0]]);
                };
                translate([0, 5, 8])
                    rotate([90, 180, 90])
                    linear_extrude(1, center=false)
                    difference() {
                        square([10, 8]);
                        translate([1.5, 2])
                            square([7, 2.5]);
                    };
            };
        };

// Passe fil
rotate([0, 0, -25])
    translate([0, dSpeaker / 2 + 10, eBase])
    rotate([90, 0, -90])
    linear_extrude(1)
    polygon([[4, 8], [2, 8], [2, 6], [0, 6], [0,0], [9,0], [9,3], [8,3]]);
rotate([0,0,-10])
// appui tringle
rotate_copy(a=[0, 0, 180], n=1)
    translate([- (dSpeaker / 2 + 8), -3, 0])
        rotate([0,-90, 0]) {
            linear_extrude(1)
            difference() {
                square([10, 6]);
                translate([10, 3])
                    circle(d=4);
            };
        // contrefort
            translate([0, 3, 1])
            rotate([90, 0, 0])
                linear_extrude(0.5, center=true)
                polygon([[0,0], [0,2], [8,0]]);
        };

// Appui circuit imprimé
rotate([0, 0, -15])
    translate([dSpeaker / 2 + 2.75, 0, 0])
    cylinder(d=3.5, h=9);

translate([dSpeaker/2, dSpeaker/2, eBase])
    rotate([0, 0, 30])
    linear_extrude(0.5)
    text("R", halign="center", valign="center", size=6);

rotate([0, 0, 55])
    translate([- 24, 0, eBase])
    rotate([0, 0, 90])
    linear_extrude(0.5)
    cc_domain_public(s=5);
